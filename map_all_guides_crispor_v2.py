# -*- coding: utf-8 -*-
"""
Created on Thursday July 6 2017
@author: vivienschoonenberg
"""
import pandas as pd
import numpy as np
import os, subprocess, argparse, multiprocessing, re, gzip, wget, csv, pickle, itertools

def main(crispor_file):
    df_crispor = pd.read_csv(crispor_file)
    # caluclate DSB coordinates right now only hg19 spCas9
    def calcDSB(row):
        strand = row['strand']
        start = row['start']
        if strand == '+':
            return start + 17
        else:
            return start + 6

    df_crispor['DSB_Coordinate'] = df_crispor.apply(calcDSB, axis=1)

    ###TODO map guides to proteins
    def mapDSB_2AA(df_crispor):
        """
        @author: vivienschoonenberg
        """
        total_DSB = len(df_crispor.index)
        df_crispor = pd.concat([df_crispor,pd.DataFrame(columns=['Amino.Acid.Coordinate', 'gene_name', 'ensembl_transcript_id', 'distance_exon_border'])])
        df_new = pd.DataFrame()
        df_not_mapped = pd.DataFrame()

        ###TODO define location ensembl_coding_coords location ('GRCh37.ensembl.coding_coords.csv.gz')
        ensembl_coding_coords = '/home/vs149/GERP/GRCh37.ensembl.coding_coords.csv.gz'
        df_coding_coords = pd.read_csv(ensembl_coding_coords, compression='gzip')
        df_coding_coords = df_coding_coords[~df_coding_coords['chromosome_name'].str.contains('PATCH')]
        df_coding_coords = df_coding_coords[~pd.isnull(df_coding_coords['cdna_coding_start'])]

    	for row in range(total_DSB):
            #get chromosome
            chrom = df_crispor.loc[row, 'chrom']
            chrom = re.sub('chr', '',chrom)
            DSB_coordinate = df_crispor.loc[row, 'DSB_Coordinate']
            #get all transcript_ids the guide maps to.
            subset_coding_coords = df_coding_coords[(df_coding_coords.chromosome_name==chrom) & (df_coding_coords.genomic_coding_start <= DSB_coordinate) & (df_coding_coords.genomic_coding_end >= DSB_coordinate)].reset_index(drop=True)

            ## note whichever guides can't be mapped??
            if subset_coding_coords.empty:
                df_not_mapped = df_not_mapped.append(df_crispor.loc[row,:])
            else:
                transcript_ids = subset_coding_coords['ensembl_transcript_id'].tolist()
                gene = subset_coding_coords.loc[0, 'external_gene_name']

            #get 2 nearest amino acids for DSB
                for transcript in transcript_ids:
                    subset_coding_coords=df_coding_coords[df_coding_coords.ensembl_transcript_id==transcript].reset_index(drop=True)
                    offset = subset_coding_coords['cdna_coding_start'].min()
                    subset_coding_coords = subset_coding_coords[(subset_coding_coords['genomic_coding_start'] <= DSB_coordinate) & (subset_coding_coords['genomic_coding_end'] >= DSB_coordinate)]
                    #by @mitch
                    if subset_coding_coords.strand.values[0] == -1:
                        diff = abs(DSB_coordinate - subset_coding_coords['genomic_coding_end'].values[0])
                        dist_exon_border = min([(subset_coding_coords['exon_chrom_start'].values[0] - DSB_coordinate), (DSB_coordinate - subset_coding_coords['exon_chrom_end'].values[0])])
                    else:
                        diff = abs(DSB_coordinate - subset_coding_coords['genomic_coding_start'].values[0])
                        dist_exon_border = min([(subset_coding_coords['exon_chrom_end'].values[0] - DSB_coordinate), (DSB_coordinate - subset_coding_coords['exon_chrom_start'].values[0])])
                    cdna_len = subset_coding_coords['cdna_coding_start'].values[0] + diff - offset - 1
                    AA1_decimal = float(cdna_len)/float(3) + 1
                    AA1 = int(AA1_decimal - 0.5)
                    AA2 = int(AA1_decimal + 0.5)
                    #by @vivienschoonenberg
                    df_crispor.loc[row, 'gene_name'] = gene
                    df_crispor.loc[row, 'ensembl_transcript_id'] = transcript
                    df_crispor.loc[row, 'Amino.Acid.Coordinate'] = AA1
                    df_crispor.loc[row, 'distance_exon_border'] = dist_exon_border
                    df_new = df_new.append(df_crispor.loc[row, :])
                    df_crispor.loc[row, 'Amino.Acid.Coordinate'] = AA2
                    df_new = df_new.append(df_crispor.loc[row, :])
        df_not_mapped.to_csv('crispor_not_mapped_v4.csv', index=False)
        return df_new

    df_crispor = mapDSB_2AA(df_crispor)
    part = crispor_file.split('.')[-2].split('_')[-1]
    df_crispor.to_csv('part_'+part+'_crispor_mapped_v2.csv.gz', index=False, compression='gzip')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", dest="crispor_file")
    args = parser.parse_args()
    main(args.crispor_file)
