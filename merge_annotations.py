# -*- coding: utf-8 -*-
import os, subprocess, argparse, multiprocessing, re, gzip, itertools
import pandas as pd
import numpy as np

def main():
    #df_annotations = pd.DataFrame()
    #for directory in os.listdir('/home/vs149/annotations_batch/'):
    #    df_temp = pd.read_csv('/home/vs149/annotations_batch/'+directory+'/hg19_annotations.csv')
    #    df_annotations=df_annotations.append(df_temp)
    #df_annotations.to_csv('/home/vs149/all_annotations_crispro_hg19_081517.csv.gz', index=False, compression='gzip')
    df_annotations=pd.read_csv('/home/vs149/all_annotations_crispro_hg19_081517.csv.gz')
    print df_annotations.head()
    print df_annotations.columns.values
    df_blast_alignments = pd.read_csv('pdb_alignments_crispro_hg19.csv.gz')
    df_crispor = pd.read_csv('crispor_mapped_hg19_v2.csv.gz')
    df_ss = pd.read_csv('/home/vs149/secondary_structure_batch/all_secstruct.csv.gz')

    df_crispor = df_crispor[~(df_crispor['Amino.Acid.Coordinate'] == 0)]
    df_crispor['distance_exon_border'] = df_crispor['distance_exon_border'].abs()
    #don't merge 'start', 'end', 'pam' from df_crispor
    crispor_cols = ['Amino.Acid.Coordinate', 'DSB_Coordinate', 'strand', 'chrom', 'guide', 'offtarget_score', 'doench_score', 'oof_score', 'ensembl_transcript_id', 'gene_name', 'distance_exon_border']
    df_crispro = pd.DataFrame()
    df_crispro = pd.merge(df_annotations, df_crispor[crispor_cols], how='left',left_on=['ensembl_transcript_id','position'], right_on=['ensembl_transcript_id','Amino.Acid.Coordinate'])
    del df_crispro['Amino.Acid.Coordinate']

    df_crispro = pd.merge(df_crispro, df_ss, how='left', left_on=['ensembl_peptide_id', 'position', 'AA'], right_on=['ENSPID', 'AA.coord', 'AA'])
    del df_crispro['ENSPID']
    del df_crispro['AA.coord']
    print df_crispro.head()

    blast_cols = ['ensembl_transcript_id', 'AA', 'position', 'PDB_id', 'PDB_description']
    df_crispro = pd.merge(df_crispro, df_blast_alignments[blast_cols], how='left', left_on=['ensembl_transcript_id', 'position', 'AA'], right_on=['ensembl_transcript_id', 'position', 'AA'])

    del df_crispro['Gene name']
    df_crispro.rename(columns={'Transcript name':'transcript_name', 'APPRIS annotation':'APPRIS'}, inplace=True)
    df_crispro['position'] = df_crispro['position'].astype(int)
    print df_crispro.head()
    print df_crispro.columns.values
    df_crispro.to_csv('/home/vs149/crispro_annotations_hg16_0817.csv.gz', index=False, compression='gzip')
main()
