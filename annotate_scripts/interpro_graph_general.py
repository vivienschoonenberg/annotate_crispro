# -*- coding: utf-8 -*-
"""
Created on Mon Nov 13 10:41:15 2017

@author: mitch
Create graph of interpro domain relationships
Collapse interpro protein entries by greatest common ancestor
Collapse related interpro entries via union
"""

import pandas as pd
import re
import argparse
import os
import numpy as np
#ftp://ftp.ebi.ac.uk/pub/databases/interpro/ParentChildTreeFile.txt

parser = argparse.ArgumentParser()
parser.add_argument("--treeFile", type=str, help="Path/to/ParentChildTreeFile.txt")
parser.add_argument("--domains", type=str, help="Path/to/GRCxNN.interpro_domains.csv.gz")
parser.add_argument("-o","--outdir", type=str, help="folder to write filtered interpro domains", default="")
parser.add_argument("--entryList", type=str, help="Path/to/entry.list")

args = parser.parse_args()
with open(args.treeFile, 'rt') as treeFile:
    # we only need to traverse up so only track parent
    graph = dict() # id: description, level, parent
    prevLevel = 0
    prev = None
    for line in treeFile:
        match  = re.match('-*', line)
        st, en = match.span()
        level = (en - st) // 2
        
        id_name, description = line[en:].split('::')[:2]
        
        if level > prevLevel: # child
            parent = prev
        elif level == prevLevel and level != 0: # sibling
            parent = graph[prev][2]
        else:
            if level == 0: # new tree
                parent = None
            else: # younger -> older
                while prevLevel > level:
                    prevLevel, prev = graph[prev][1:]
                parent = prev
            
        graph[id_name] = [description, level, parent]
        
        prev = id_name
        prevLevel = level

df = pd.DataFrame.from_dict(graph, orient='index')
df.columns = ['description','level','parent']
df.index.name = 'id'

df_domain = pd.read_csv(args.domains)
df_interpro = pd.read_table(args.entryList)

df_interpro = df_interpro[df_interpro['ENTRY_TYPE'].str.contains('Domain|Repeat')]
df_domain = pd.merge(df_domain, df_interpro, how='inner',
                     left_on='interpro', right_on='ENTRY_AC')
df = df[df.index.isin(df_interpro['ENTRY_AC'])]

def getEldestAncestor(id_name):
    if id_name in df.index:
        while graph[id_name][-1] != None:
            id_name = graph[id_name][-1]
        return id_name
    else:
        return id_name
df_domain['ancestor'] = df_domain['interpro'].apply(getEldestAncestor)
df_domain['isDesc'] = df_domain['ancestor'] != df_domain['interpro']
df_domain = pd.merge(df_domain, df, how='left', left_on='interpro',
                     right_index=True)
df_domain['level'] = df_domain['level'].fillna(0).astype(int)

data = []
df_domain.sort_values(['ensembl_peptide_id','ancestor','interpro_start','interpro_end'],
                      inplace=True)
for name, group in df_domain.groupby(['ensembl_peptide_id','ancestor'],
                                     sort=False):
    pep, ancestor = name
    intervals = group[['interpro_start','interpro_end', 'interpro']].values
    prevSt, prevEn, interpro = intervals[0]
    ids = {interpro}
    for st, en, interpro in intervals[1:]:
        if st < prevEn: # intersect
            prevEn = max(prevEn, en)
            ids.add(interpro)
        else: # do not intersect
            data.append([pep, prevSt, prevEn, list(ids)])
            prevSt, prevEn = st, en
            ids = {interpro}
    data.append([pep, prevSt, prevEn, list(ids)])   
df_union = pd.DataFrame(data, columns=['ensembl_peptide_id', 'start', 'end',
                                       'id_list'])
df_interpro.set_index('ENTRY_AC', inplace=True)
df_interpro_short = df_domain[['interpro','interpro_short_description']].drop_duplicates(subset=['interpro'])
df_interpro_short.set_index('interpro', inplace=True)
dtype = [('id','U10'),('description','U200'),('level',int)]
def getIDannots(ids):
    data = []
    for id_name in ids:
        desc = df_interpro_short.at[id_name, "interpro_short_description"]
        if name in graph:
            data.append(tuple([id_name, desc] + graph[id_name][1:-1]))
        else:
            data.append(tuple([id_name, desc, 0]))
    lst = np.sort(np.array(data, dtype=dtype), order='level')
    return '::'.join([desc for _,desc,_ in lst]), '::'.join([ids for ids,_,_ in lst])
df_union[['description','ids']] = df_union['id_list'].apply(getIDannots).apply(pd.Series)
del df_union['id_list']
def getInterproType(ids):
    id1 = ids.split('::')[0]
    return df_interpro.at[id1,"ENTRY_TYPE"]

df_union['type'] = df_union['ids'].apply(getInterproType)
df_union['type_cat'] = df_union['type'].apply(lambda x: int(x == 'Domain'))
data_levels = []
df_union.sort_values(['ensembl_peptide_id','type','start','end'], inplace=True)
for pep, group in df_union.groupby(['ensembl_peptide_id', 'type'], sort=False):
    level_dict = dict() # key is row in annotation plot, value list of domains
    max_level = 0
    for row in group.itertuples(index=False,name=None):
        level = 0
        if not level_dict:
            level_dict[max_level] = [list(row)]
        else:
            while level <= max_level:
                if row[1] >= level_dict[level][-1][2]:
                    level_dict[level] += [list(row)]
                    break
                level += 1
            if level > max_level:
                level_dict[level] = [list(row)]
                max_level = level
    data_levels += [val + [key] for key, vals in level_dict.items() for
                    val in vals]
df_union2 = pd.DataFrame(data_levels, columns = df_union.columns.tolist() + ['level'])
del df_union
df_union_domain_max = df_union2[df_union2['type'] == 'Domain'].groupby(
        'ensembl_peptide_id').agg({'level':'max'})
df_union3 = pd.merge(df_union2, df_union_domain_max, how='left',
                     left_on='ensembl_peptide_id', right_index=True)
df_union3['level_y'].fillna(-1, inplace=True)
df_union3['level'] = df_union3.apply(lambda row: row['level_x'] if
         row['type'] == 'Domain' else row['level_x'] + row['level_y'] + 1,
         axis=1)
# clean dataframe
del df_union2
df_union3 = df_union3[df_union3.columns.tolist()[:6] + ['level']]
df_union3[['start','end']] = df_union3[['start','end']].astype(int)
df_domain2 = df_domain.iloc[:,0:4]
df_domain2.drop_duplicates(subset=['ensembl_peptide_id'],inplace=True)
df_union4 = df_union3.merge(df_domain2, how='left', on='ensembl_peptide_id')
df_union4 = df_union4[df_domain2.columns.tolist() +
                      df_union3.columns.tolist()[1:]]
outfile = os.path.join(args.outdir, os.path.basename(args.domains).replace(".csv","-filtered.csv"))
df_union4.to_csv(outfile,
                 index=False, compression='gzip')