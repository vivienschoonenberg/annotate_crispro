#! /usr/bin/env Rscript
############################################################
## Date created: Wednesday 10 March 2021
##
## Author(s): Mitchel A. Cole, Vivien A. C. Schoonenberg
##
## Email: v.schoonenberg@imb-mainz.de
##
############################################################

library(biomaRt)
library(readr)
library(PFAM.db)
args <- commandArgs(trailingOnly = TRUE) #hsapiens 38 hg38 20210310_ensembl_annots tfiid-saga_gene_ids.txt tfiid-saga
print(args[1:2])
organism <- args[1]
#organism <- "hsapiens"
version <- as.numeric(args[2]) #eg 38,37
#version <- as.numeric(38)
ucsc_version <- args[3] #eg hg19, mm10
#ucsc_version <- "hg38"
dir <- args[4]
#dir <- "20210310_ensembl_annots"
gene_file <- args[5] #"tfiid-saga_gene_ids.txt"
#gene_file <- "tfiid-saga_gene_ids.txt"
set_name <- args[6]
#set_name <- "tfiid-saga"


dir.create(dir, showWarnings = FALSE)
print("Downloading Ensembl Annotations")
if (version == 37){
  ensembl <- useMart("ensembl", dataset = sprintf('%s_gene_ensembl',organism), GRCh = version)
} else{
  ensembl <- useMart("ensembl", dataset = sprintf('%s_gene_ensembl',organism))
}
# can filter for gene IDs
gene_ids <- scan(gene_file, character(), sep="\n")
# save genomic_coordinates start and end of each gene to filter CRISPOR downloads.
genomic_coords <- getBM(attributes = c('ensembl_gene_id', 'chromosome_name','start_position','end_position'), filters = "ensembl_gene_id", values = gene_ids, mart = ensembl)
df_pfam <- getBM(attributes = c('external_gene_name','ensembl_gene_id','ensembl_transcript_id','ensembl_peptide_id','pfam','pfam_start','pfam_end'), filters = c('transcript_biotype', "ensembl_gene_id"), values = list(c('protein_coding'), gene_ids), mart = ensembl)
df_smart <- getBM(attributes = c('external_gene_name','ensembl_gene_id','ensembl_transcript_id','ensembl_peptide_id', 'smart','smart_start','smart_end'), filters = c('transcript_biotype', "ensembl_gene_id"), values = list(c('protein_coding'), gene_ids), mart = ensembl)
df_interpro <- getBM(attributes = c('external_gene_name','ensembl_gene_id','ensembl_transcript_id','ensembl_peptide_id',
                                    'interpro','interpro_start','interpro_end','interpro_short_description'), 
                     filters = c('transcript_biotype', "ensembl_gene_id"), values = list(c('protein_coding'), gene_ids), mart = ensembl)
df_exon <- getBM(attributes = c('external_gene_name','ensembl_gene_id','ensembl_transcript_id','ensembl_peptide_id','rank',
'chromosome_name','strand','genomic_coding_start','genomic_coding_end','cdna_coding_start','cdna_coding_end'), filters = c('transcript_biotype', "ensembl_gene_id"), values = list(c('protein_coding'), gene_ids), mart = ensembl)
df_exon2 <- getBM(attributes = c('ensembl_transcript_id','external_transcript_name', 'transcript_appris', 'uniprot_gn_id'),
                  filters = c('transcript_biotype', "ensembl_gene_id"), values = list(c('protein_coding'), gene_ids), mart = ensembl)
df_exon_final <- merge(df_exon, df_exon2, by='ensembl_transcript_id', all.x = TRUE)
df_exon_final <- df_exon_final[order(df_exon_final$ensembl_transcript_id,df_exon_final$rank),]
pfamNames <- as.list(PFAMID)
df_pfam$pfam_description <- as.character(pfamNames[df_pfam$pfam])

df_smart_ids <- read.table('smart_ids.txt', row.names = 2)
df_smart$smart_description <- df_smart_ids[df_smart$smart,'DOMAIN']

write_csv(df_pfam, file.path(dir, sprintf("GRC%s%s.%s.%s.pfam.csv.gz",substr(organism,1,1),version,organism, set_name)))
write_csv(df_smart, file.path(dir, sprintf("GRC%s%s.%s.%s.smart.csv.gz",substr(organism,1,1),version,organism, set_name)))
write_csv(df_interpro, file.path(dir, sprintf("GRC%s%s.%s.%s.interpro.csv.gz",substr(organism,1,1),version,organism, set_name)))
write_csv(df_exon_final, file.path(dir, sprintf("GRC%s%s.%s.%s.exons.csv.gz",substr(organism,1,1),version,organism, set_name)))

#only download the CRISPOR annotations if we did not already :)
#if (!file.exists(sprintf("GRC%s%s.%s.crispor.bed",substr(organism,1,1),version,organism))){
  #download crispor data
#  print("Downloading CRISP-OR Annotations")
#  system(sprintf("rsync -avzP rsync://hgdownload.soe.ucsc.edu/gbdb/%s/crisprAll/crispr.bb .",ucsc_version))
  #convert to bed 
#  print("Converting CRISP-OR Annotations to Bed File")
#  system(sprintf("bigBedToBed crispr.bb GRC%s%s.%s.crispor.bed",substr(organism,1,1),version,organism))
#  system("rm -f crispr.bb")
#}

for (row in 1:nrow(genomic_coords)){
  chrom <- genomic_coords[row, "chromosome_name"]
  start <- genomic_coords[row, "start_position"]
  end <- genomic_coords[row, "end_position"]
  file <- paste0(dir, '/', genomic_coords[row, "ensembl_gene_id"], ".crispor.bed")
  system(sprintf("bigBedToBed http://hgdownload.soe.ucsc.edu/gbdb/%s/crisprAll/crispr.bb -chrom=chr%s -start=%s -end=%s %s", ucsc_version, chrom, start, end, file))
}

crispor_cols <- c('chrom','chromStart','chromEnd','name','score','strand','thickStart','thickEnd','reserved','_crisprScanColor',
                  '_specColor','guideSeq','pam','scoreDesc','fusi','crisprScan','doench','oof','_mouseOver','_offset')
filenames=list.files(path=paste0("./", dir), pattern = "*.crispor.bed", full.names=TRUE)
datalist = lapply(filenames, function(x){as.data.frame(read_tsv(file=x,col_names = crispor_cols))})
df_crispor <- Reduce(function(x,y) {rbind(x,y)}, datalist)
#clean up data set
df_crispor <- df_crispor[,c('chrom','chromStart','chromEnd','strand','guideSeq','pam','scoreDesc','fusi','oof')]
print("Writing CRISP-OR Annotations to disk")
write_csv(df_crispor, file.path(dir,sprintf("GRC%s%s.%s.%s.crispor.csv.gz",substr(organism,1,1),version,organism, set_name)))
system(sprintf("rm -f %s/ENSG*.crispor.bed",dir))
print("Downloading CDS")
cds_seqs <- getSequence(id = unique(df_exon_final$ensembl_peptide_id),
                    type='ensembl_peptide_id',
                    seqType = 'coding',
                    mart = ensembl)
gz_cds <- gzfile(file.path(dir,sprintf('GRC%s.%s.%s.%s.cds.fa.gz', substr(organism,1,1), version, organism, set_name)))
exportFASTA(cds_seqs, gz_cds)
close(gz_cds)
print("Downloading Peptide Sequences")
pep_seqs <- getSequence(id = unique(df_exon_final$ensembl_peptide_id),
                        type='ensembl_peptide_id',
                        seqType = 'peptide',
                        mart = ensembl)
gz_pep <- gzfile(file.path(dir,sprintf('GRC%s.%s.%s.%s.pep.fa.gz', substr(organism,1,1), version, organism, set_name)))
exportFASTA(pep_seqs, gz_pep)
close(gz_pep)