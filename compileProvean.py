# -*- coding: utf-8 -*-
"""
Created on Tue Mar 20 14:33:07 2018

@author: mitch

proveanCompile.py

Converts directory full of single peptide provean output to dataframe
"""

import pandas as pd
import os

path = r"E:\projects\crispro\test\taka\provean"
dfs = []
for file in os.listdir(path):
    with open(os.path.join(path, file), "rt") as infile:
        try:
            line = next(infile)
            while "## PROVEAN scores ##" not in line:
                line = next(infile)
            df_temp = pd.read_table(infile)
            df_temp["ensembl_peptide_id"] = file.split(".")[0]
            dfs.append(df_temp)
        except StopIteration:
            print(file.split(".")[0])
df = pd.concat(dfs)
df[["AA","pos"]] = df["# VARIATION"].str.extract("(\S)(\d+)", expand=True)
df["pos"] = df["pos"].astype(int)
df.drop("# VARIATION", axis=1, inplace=True)
df.rename(columns={"SCORE":"provean_score"}, inplace=True)
df = df[["ensembl_peptide_id","pos","AA","provean_score"]]
df.to_csv(os.path.join(path,"provean_del.csv.gz"),
          index=False,
          na_rep="NA",
          compression="gzip")