#!/bin/sh

display_usage() {
    echo -e "\nUsage:\n ./CRISPROANNOTATION.sh [-h] fasta org_name ucsc_version ensembl_version tmp-dir out-dir\npositional arguments:\n\tfasta\t fasta file of peptides. header in ensembl format\n\torg_name\torganism genus abbreviation and species (i.e. h_sapiens for human, m_musculus for mouse)\n\tucsc_version\tucsc organism abbreviation (i.e. hg19, mm10)\n\tensembl_version\toptions(numeric, i.e. 92, 'GRCh37','LATEST')\n\ttmp-dir\tLocation for temporary files\n\tout-dir\tLocation for final output"
}
if [ $# -le 4 ]
then
    display_usage
    exit 1
fi
if [[ ($# == "--help") || $# == "-h" ]]
then
    display_usage
    exit 0
fi

FA=$1
NAME=$2 #h_sapiens, m_musculus
UCSCVERSION=$3
ENSEMBLVERSION=$4
TMP=$5

# extract protein coding sequences
#@1,0,cleanFasta,FA,sbatch -p short -n 1 -t 30:0 --mem=8G
NUMSEQS="$(python filterFasta.py $FA $NAME $TMP)"
FILTERFA='$TMP/$NAME_protein_coding_peps.fa'

#@2,1,downloadAnnotation,FILTERFA,sbatch -c 1 -t 0-12:00 -p short --mem=16G
srun -c 1 Rscript getEnsemblAnnots.R $NAME $ENSEMBLVERSION $UCSCVERSION $TMP

# run provean and disorder
BATCHSIZE=$(expr $NUMSEQS / 10000)

if [ $BATCHSIZE -lt 1 ]
then 
    BATCHSIZE=1
fi
NUMJOBS=$(expr $NUMSEQS / $BATCHSIZE + 1)

#@3,2,provean,FILTERFA,sbatch -c 4 --array=1-$NUMJOBS -t 0-24:00 -p medium --mem=40G
srun python provean_batch.py $FILTERFA $SLURM_ARRAY_TASK_ID $BATCHSIZE --temp-dir $TMP --out-dir $TMP --threads 4

#@4,2,disorder,FILTERFA,sbatch -c 4 --array=1-$NUMJOBS -t 0-12:00 -p short --mem=40G
srun python disorder_batch.py $FILTERFA $SLURM_ARRAY_TASK_ID $BATCHSIZE --temp-dir $TMP --out-dir $TMP --threads 4

