# -*- coding: utf-8 -*-
import os, subprocess, argparse, multiprocessing, re, gzip, itertools
import pandas as pd
import numpy as np
from Bio import SeqIO
from Bio.Alphabet import IUPAC
from Bio.Blast.Applications import NcbiblastpCommandline
from Bio.Blast import NCBIXML
from Bio import pairwise2
from Bio.pairwise2 import format_alignment
from Bio.SubsMat.MatrixInfo import blosum62
from Bio.PDB import *
from functools import partial

def main(crispro_file, outdir):
    #df_domain_seq = pd.read_hdf('/home/vs149/crispro_annotations.GRCh37.h5')
    #print df_domain_seq.head()
    #everything that is blasted is dependend on df_crsipro, split this one up in 2 proteins per file??
    #df_domain_seq = df_domain_seq[['ensembl_transcript_id', 'AA','Smart_Domain','Pfam_Domain', 'position']]
    #df_domain_seq.drop_duplicates(subset=['ensembl_transcript_id','position'],inplace=True)
    #df_domain_seq.reset_index(drop=True, inplace=True)
    #print df_domain_seq.head()
    #for transcript, group in df_domain_seq.groupby('ensembl_transcript_id'):
    #    outdir = os.path.join('/home/vs149/BLAST_batch',transcript)
    #    os.mkdir(outdir)
    #    outfile =os.path.join(outdir, transcript+'.csv')
    #    df_temp = group
    #    df_temp.to_csv(outfile)
    #BLAST tresholds
    E_VALUE_TRESH = 0.05
    IDENTITY_TRESH = 0.7
    #COVERAGE_TRESH = 0.6

    def blastPDB(seq, transcript, domain, df_output, outdir):
        save_file = os.path.join(outdir,"temp_blast_results.xml")
        #BLAST
        #blastp_cline = NcbiblastpCommandline(query=seq, db="pdbaa", matrix='BLOSUM62', gapopen=11,gapextend=1, word_size=6, comp_based_stats=2, outfmt=5, out=save_file)
        blastp_cline = NcbiblastpCommandline(query=seq, db="pdbaa", matrix='BLOSUM62', gapopen=11, gapextend=1, word_size=6, outfmt=5, out=save_file)
        print blastp_cline
        #stdout, stderr = blastp_cline()
        blastp_cline()
        #parse results
        blast_records = NCBIXML.parse(open(raw_input(save_file),"rU"))
        #with open(save_file, 'rt') as result_handle:
        #print save_file
        #result_handle = open(save_file, "r")
        #print result_handle
        #blast_records = NCBIXML.parse(result_handle)
        for blast_record in blast_records:
            query_len = blast_record.query_length
            for alignment in blast_record.alignments:
                title = alignment.title
                for hsp in alignment.hsps:
                        e_val = hsp.expect
                        identity = hsp.identities
                        align_len = hsp.align_length
                        p_identity = float(identity) / float(align_len)
                        p_coverage = float (align_len) / float(query_len)
                        alignment_start = hsp.query_start
                        alignment_end = alignment_start+align_len
                        if e_val <= E_VALUE_TRESH and p_identity >= IDENTITY_TRESH:
                            accession = alignment.accession
                            accession_split = accession.split('_')
                            PDB = accession_split[0]
                            chain = accession_split[1]
                            results_dict = {'transcript':str(transcript),
                                            'domain':str(domain),'PDB':str(PDB),
                                            'chain':str(chain),
                                            'alignment_start':int(alignment_start),
                                            'alignment_end':int(alignment_end),
                                            'alignment_length':int(align_len),
                                            'query_length':int(query_len),
                                            'e-value':str(e_val),
                                            'identity':float(p_identity),
                                            'coverage':float(p_coverage),
                                            'description':str(title)}
                            df_new_row = pd.DataFrame(results_dict, index=[0])
                            df_output = pd.concat([df_output,df_new_row])
        return df_output

    #open 1 file to save output of BLAST (can also be a dataframe, adding every row??)
    df_outputBLAST = pd.DataFrame(columns=['transcript', 'domain', 'PDB',
                                           'chain', 'alignment_start', 'alignment_end',
                                           'alignment_length', 'query_length',
                                           'e-value', 'identity', 'coverage', 'description'])
    #get complete sequence and seperate sequences for each of the domains.
    df_crispro = pd.read_csv(crispro_file)
    df_domain_seq = df_crispro[['ensembl_transcript_id', 'AA', 'Smart_Domain',
                                'Pfam_Domain', 'position']]
    df_domain_seq.drop_duplicates(subset=['ensembl_transcript_id','position'],
                                  inplace=True)
    df_domain_seq.reset_index(drop=True, inplace=True)
    #dict_transcripts = {key: df_domain_seq.loc[value] for key, value in df_domain_seq.groupby('ensembl_transcript_id').groups.items()}
    #for key in dict_transcripts.keys():
    for transcript, group in df_domain_seq.groupby('ensembl_transcript_id'):
        #df_temp = dict_transcripts[key]
        df_temp = group
        #transcript = key
        complete_seq = ''.join(df_temp['AA'].tolist())
        domain = 'complete sequence'
        seq_file = os.path.join(outdir,"temp_blast_sequence.txt")
        with open(seq_file, "w") as text_file:
            text_file.write("{0}".format(complete_seq))
        #BLAST here
        df_outputBLAST = blastPDB(seq_file,transcript,domain,df_outputBLAST, outdir)
        #dict_domains = {key: df_temp.loc[value] for key, value in df_temp.groupby('Smart_domain').groups.items()}
        for domain,group_domain in df_temp.groupby('Smart_Domain'):
            #some proteins have multiple of the same domain. These need to be split as well.
            #determine last AA of the proteins
            #domain = key1
            df_temp2 = group_domain
            df_temp2.reset_index(drop=True, inplace=True)
            print df_temp2
            list_pos = df_temp2['position'].tolist()
            cutoffs = [min(list_pos)]
            for i in range(len(list_pos)-1):
                if (list_pos[i]+1) != list_pos[i+1]:
                    cutoffs.append(list_pos[i])
                    cutoffs.append(list_pos[i+1])
            cutoffs.append(list_pos[-1])
            print cutoffs
            for i in range(0,len(cutoffs),2):
                start = cutoffs[i]
                end = cutoffs[i+1]
                df_temp3 = df_temp2.iloc[df_temp2[df_temp2.position==start].index[0]:(df_temp2[df_temp2.position==end].index[0]+1)]
                domain_seq = ''.join(df_temp3['AA'].tolist())
                with open(seq_file, "w") as text_file:
                    text_file.write("{0}".format(domain_seq))
                #BLAST here
                df_outputBLAST=blastPDB(seq_file,transcript,domain,df_outputBLAST, outdir)

    df_oBLAST_temp = df_outputBLAST[df_outputBLAST['domain'] == 'complete sequence'].reset_index(drop=True)
    #For annotating all PDB files for each of the protein isoforms just use BLAST for complete sequence and its alignment (which should be close to the real alignment)
    df_crispro['PDB_id'] = 'NA'
    df_crispro['PDB_description'] ='NA'
    index_length = len(df_oBLAST_temp.index)
    for i in range(index_length):
        df_crispro.ix[(df_crispro['ensembl_transcript_id']==df_oBLAST_temp.loc[i,'transcript']) & (df_crispro['position']>=df_oBLAST_temp.loc[i,'alignment_start']) & (df_crispro['position']<=df_oBLAST_temp.loc[i,'alignment_end']), 'PDB_id'] = df_oBLAST_temp.loc[i,'PDB']
        df_crispro.ix[(df_crispro['ensembl_transcript_id']==df_oBLAST_temp.loc[i,'transcript']) & (df_crispro['position']>=df_oBLAST_temp.loc[i,'alignment_start']) & (df_crispro['position']<=df_oBLAST_temp.loc[i,'alignment_end']), 'PDB_description'] = df_oBLAST_temp.loc[i,'description']
    df_outputBLAST = df_outputBLAST.drop_duplicates(['PDB'], keep='last')
    df_outputBLAST = df_outputBLAST.reset_index(drop=True)
    crispro_out = os.path.join(outdir, "pdb_annotations.csv")
    blast_all_out = os.path.join(outdir,"complete_BLAST_result.csv")
    df_crispro.to_csv(crispro_out)
    df_outputBLAST.to_csv(blast_all_out)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("-odir", "--output_directory", help="full path to the directory in which all results will be saved here", dest="outdir")
	parser.add_argument("-f", "--in_file", help="input csv annotations crispro (part)", dest="infile")
	args = parser.parse_args()
	main(args.infile, args.outdir)
