# using DescribePROT: https://doi.org/10.1093/nar/gkaa931

#human: http://biomine.cs.vcu.edu/servers/DESCRIBEPROT/download_database/9606_database.json

#mouse: http://biomine.cs.vcu.edu/servers/DESCRIBEPROT/download_database/10090_database.json

#describePROT_hsapiens_9606_database.json
import json
import pandas as pd

with open('describePROT_hsapiens_9606_database.json') as f:
    data = json.loads("[" + 
        f.read().replace("}\n{", "},\n{") + 
    "]")

df = pd.DataFrame(data)
df.to_csv("describePROT_hsapiens_9606_database.csv.gz", compression="gzip")
df_01 = df.loc[:, df.columns.str.contains('ACC|PSIPRED|VSL2|seq')]
df_01.to_csv("describePROT_slice_hsapiens_9606_database.csv.gz", compression="gzip")

# df.columns
# Index(['ACC', 'ACC_entry', 'ASAquick_rawscore', 'DFLpredScore',
#        'DRNApredDNAscore', 'DRNApredRNAscore', 'DisoDNAscore', 'DisoPROscore',
#        'DisoRNAscore', 'MMseq2_conservation_level',
#        'MMseq2_conservation_score', 'MMseqs2_pssm', 'MoRFchibiScore',
#        'PSIPRED_helix', 'PSIPRED_strand', 'ProteinName', 'SCRIBERscore',
#        'SignalP_score', 'VSL2_score', 'seq', 'seqlength', 'taxonomy_id'],
#       dtype='object')

#read input from describePROT
df_descript = pd.read_csv("./data_files/describePROT_slice_hsapiens_9606_database.csv.gz")
#read exons - contains uniprot ids
df_exon = pd.read_csv("./20210312_ensembl_annots/GRCh38.hsapiens.tfiid-saga.exons.csv.gz")
# select descriptions for selected proteins
upx = list(df_exon['uniprot_gn_id'].unique())
df_descript_filt = df_descript[df_descript['ACC'].isin(upx)]

# now need to "melt" some columns -> linear annotation of the protein. 
df_descript_filt.columns

