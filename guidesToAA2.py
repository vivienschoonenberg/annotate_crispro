# -*- coding: utf-8 -*-
"""
Created on Tue Mar 20 14:58:29 2018

Author(s): Mitchel A. Cole, Vivien A. C. Schoonenberg

guidesToAA.py

Maps guides to AA
Merges provean_del.csv with CRISPOR database
"""

import pandas as pd
import os
from Bio import SeqIO
import argparse
import multiprocessing
import gzip 

parser = argparse.ArgumentParser()
parser.add_argument("--tmp-dir", type=str,
                    help="Directory of input files")
parser.add_argument("--organism", type=str,
                    help="organism, genus_initial_species" +\
                    " i.e. h_sapiens, m_musculs")
parser.add_argument("--ucsc_version", type=str,
                    help='ucsc version for genome assembly' +\
                    ' i.e hg19, hg38, mm10')

args = parser.parse_args()

#get files
def getFileByEnding(directory, starting, ending):
    files = [file for file in os.listdir(directory) if file.endswith(ending) and file.startwith(starting)]
    if files:
        return files[0]
    else:
        return None
cripsor_file = os.path.join(args.tmp_dir,"{}.crispor.csv.gz".format(args.ucsc_version))
pep_file = os.path.join(args.tmp_dir, getFileByEnding(args.tmp_dir, args.organism, "pep.fa.gz"))
cds_file = os.path.join(args.tmp_dir, getFileByEnding(args.tmp_dir, args.organism, "cds.fa.gz"))
exon_file = os.path.join(args.tmp_dir, getFileByEnding(args.tmp_dir, args.organism, "exons.csv.gz"))
domain_file = os.path.join(args.tmp_dir, getFileByEnding(args.tmp_dir, args.organism, "interpro.csv.gz"))

provean_file = os.path.join(args.tmp_dir, getFileByEnding(args.tmp_dir, args.organism, "provean_del.csv.gz"))

df_exons = pd.read_csv(exon_file)
df_provean = pd.read_csv(provean_file)
id_cols = ["ensembl_gene_id","ensembl_transcript_id","ensembl_peptide_id",
                   "external_gene_name","external_transcript_name"]
if "transcript_appris" in df_exons.columns:
    id_cols += ["transcript_appris"]
df_ids = df_exons[id_cols].drop_duplicates(subset="ensembl_peptide_id")
df_exons["Exon_Length"] = df_exons["exon_chrom_end"] - df_exons["exon_chrom_start"] + 1
df_provean = pd.merge(df_ids, df_provean, how='right', on='ensembl_peptide_id')
df_provean["gene_fraction"] = df_provean.groupby("ensembl_peptide_id")["pos"].transform(
        lambda s: s/s.max())
## add codons
with gzip.open(os.path.join(genome_path,"Mus_musculus.GRCm38.cds.all.fa.gz"), "rt") as infa:
    cds_dict = SeqIO.to_dict(SeqIO.parse(infa, "fasta"))
cds_dict = {key.split('.')[0] : str(value.seq) for key,value in cds_dict.items()}
def getCodon(row):
    txn = row["ensembl_transcript_id"]
    if txn in cds_dict:
        seq = cds_dict[row["ensembl_transcript_id"]]
        pos = row["pos"]
        codon = seq[(pos - 1) * 3: pos * 3]
        return codon
    else:
        return pd.np.nan
df_provean["codon"] = df_provean.apply(lambda row: getCodon(row), axis=1)
df_provean = df_provean[df_provean["codon"].notnull()]
## add exons
### create NT df
nt_data = []
for row in df_exons.itertuples(index=False):
    enst = row[0]
    chrom = row[5]
    strand = row[6]
    exon = row[4]
    genomic_start = row[9]
    genomic_end = row[10]
    exon_start = row[7]
    exon_end = row[8]
    if strand > 0:
        coords = range(genomic_start,genomic_end + 1)
        five_ends = [coord - exon_start for coord in coords]
        three_ends = [exon_end - coord for coord in coords]
    else:
        coords = range(genomic_end, genomic_start - 1, -1)
        five_ends = [exon_end - coord + 1 for coord in coords]
        three_ends = [coord - exon_start + 1 for coord in coords]
    nt_data += [[enst, exon, chrom, coord, five, three] for coord, five, three in
                zip(coords, five_ends, three_ends)]
df_nt = pd.DataFrame(nt_data, columns=["ensembl_transcript_id",
                                       "Exon",
                                       "chromosome_name",
                                       "coord",
                                       "distance_5_exon_border",
                                       "distance_3_exon_border"])
df_nt["cdna_pos"] = df_nt.groupby("ensembl_transcript_id", sort=False).cumcount() + 1
df_nt["aa_pos"] = ((df_nt["cdna_pos"] - 1) // 3) + 1
df_exons2 = df_nt[["ensembl_transcript_id","aa_pos","Exon"]].rename(
        columns={"aa_pos":"pos","Exon":"exon"})
df_exons2 = pd.merge(df_exons2, df_exons[["ensembl_transcript_id","rank","Exon_Length","chromosome_name"]],
                     how="left",
                     left_on=["ensembl_transcript_id","exon"],
                     right_on=["ensembl_transcript_id","rank"])
df_exons2.drop_duplicates(inplace=True)
df_provean2 = pd.merge(df_provean, df_exons2[["ensembl_transcript_id","exon","pos","Exon_Length","chromosome_name"]],
                      how="left",
                      on=["ensembl_transcript_id","pos"])
df_provean2.drop_duplicates(subset=["ensembl_transcript_id","pos"],inplace=True)
df_crispor = pd.read_csv(os.path.join(genome_path,
                                      "GRCh38.mmusculus.crispor.csv.gz"))
# clean up crispor
def getDSB(row):
    #  the crispor DF is 0 indexed
    strand = row["strand"]
    if strand == "+":
        dsb = row["chromStart"] + 17
    else:
        dsb = row["chromStart"] + 7
    return dsb
df_crispor["DSB_coordinate"] = df_crispor[["chromStart","strand"]].apply(getDSB, axis=1)
df_crispor["chrom"] = df_crispor["chrom"].apply(lambda x: x.split("chr")[-1])
df_crispor["scoreDesc"] = pd.to_numeric(df_crispor["scoreDesc"], errors="coerce")
df_crispor["fusi"] = df_crispor["fusi"].str.extract(".*\((\d+)\)",
          expand=False)
df_crispor.rename(columns={"chrom":"chromosome_name",
                           "fusi":"doench_score",
                           "scoreDesc":"offtarget_score",
                           "guideSeq":"guide"}, inplace=True)
df_crispor.drop(labels=["chromStart","chromEnd"], axis=1,
                inplace=True)
df_crispor["coord"] = df_crispor["DSB_coordinate"] - 2
df_crispor2 = df_crispor.copy()
df_crispor2["coord"] = df_crispor["DSB_coordinate"] + 1
df_crispor_final = pd.concat([df_crispor, df_crispor2])

# clean up df_nt
df_nt_final = pd.merge(df_nt, df_crispor_final, how="left",
                       on = ["chromosome_name", "coord"])
df_nt_final.drop(labels=["coord","cdna_pos","Exon","chromosome_name"], axis=1,
                 inplace=True)
df_nt_final.rename(columns={"aa_pos":"pos"}, inplace=True)
df_nt_final = df_nt_final[df_nt_final["guide"].notnull()]
df_provean3 = pd.merge(df_provean2, df_nt_final, how="left",
                       on=["ensembl_transcript_id","pos"])
df_interpro = pd.read_csv(os.path.join(genome_path,"GRCm38.mmusculus.interpro-filtered.csv.gz"))

interpro_data = []
for idx, row in df_interpro[df_interpro["type"]=='Domain'].iterrows():
   for pos in range(row['start'],row['end'] + 1):
       interpro_data.append([row['ensembl_peptide_id'], pos, row['description']])
df_interpro_long = pd.DataFrame(interpro_data, columns=["ensembl_peptide_id","position","Interpro_Description"])
df_interpro_long2 = df_interpro_long.groupby(["ensembl_peptide_id","position"]).agg(lambda g: g.str.cat(sep=";"))
df_provean4 = pd.merge(df_provean3, df_interpro_long2, how="left",
                       left_on=["ensembl_peptide_id", "pos"],
                       right_index=True)
df_numTxns = df_provean4.groupby(["ensembl_gene_id","guide"])["ensembl_transcript_id"].apply(
        lambda s: s.unique().shape[0]).to_frame()
df_numTxns.rename(columns={"ensembl_transcript_id":"targeted_transcripts"},
                  inplace=True)
df_provean5 = pd.merge(df_provean4, df_numTxns, how="left",
                       left_on=["ensembl_gene_id","guide"],
                       right_index=True)
df_provean5["num_transcripts"] = df_provean5.groupby("ensembl_gene_id")["ensembl_transcript_id"].transform(
        lambda s: s.unique().shape[0])
df_provean5["targeted_tarnscripts_frac"] = df_provean5["targeted_transcripts"] / df_provean5["num_transcripts"]

new_cols = ["ensembl_gene_id","ensembl_transcript_id","ensembl_peptide_id",
            "gene_name","transcript_name","APPRIS","position","AA","codon",
            "guide","chrom","DSB_Coordinate","strand","offtarget_score",
            "doench_score","oof_score","gene_fraction","Exon","Exon_Length",
            "distance_5_exon_border","distance_3_exon_border",
            "Exon_Multiple_of_3","escapeNMD","num_transcripts",
            "targeted_transcripts","targeted_transcripts_frac","provean_score",
            "disorder_score","SecStruct","Interpro_Description","PDB_id",
            "PDB_description"]
def escapeNMD(row):
    if pd.isnull(row['guide']):
        return pd.np.nan
    elif row['exon'] == row['last_exon']:
        return True
    elif row['exon'] == row['last_exon'] - 1 and row['distance_3_exon_border'] <= 55:
        return True
    else:
        return False
df_provean5["last_exon"] = df_provean5.groupby("ensembl_transcript_id")["exon"].transform(lambda s: s.max())
df_provean5["escapeNMD"] = df_provean5.apply(escapeNMD, axis=1)
df_provean5["Exon_Multiple_of_3"] = (df_provean5["Exon_Length"] % 3) == 0 
