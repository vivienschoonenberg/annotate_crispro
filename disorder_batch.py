from Bio import SeqIO
import argparse 
import subprocess
import os
import gzip
import multiprocessing

parser = argparse.ArgumentParser(description='Batch jobs '+
                                 'for Disorder Scores')
parser.add_argument('fasta', type=str,
                    help="Fasta File of Peps")
parser.add_argument('n', type=int, nargs=1,
                    help='Job number')
parser.add_argument('batchsize', type=int,
                    help='Number of peptides to process per job')
parser.add_argument('--temp-dir', type=str,
                    help="Location for Temporary Files")
parser.add_argument('--out-dir', type=str,
                    help="Location for output")
parser.add_argument('--threads', type=int, nargs=1, default=1,
                    help='Number of threads')
args = parser.parse_args()
n = args.n[0] - 1
threads = args.threads[0]
fasta_pep = args.fasta
script_dir = os.path.dirname(os.path.realpath(__file__))
if fasta_pep.endswith(".gz"):
    open_fun = gzip.open
else:
    open_fun = open
with open_fun(fasta_pep, 'rt') as pepfa:
    pep_dict = SeqIO.to_dict(SeqIO.parse(pepfa, "fasta"))

enspids = list(pep_dict.keys())
tmpdir = args.temp_dir
outdir = args.outdir

try:
    os.mkdir(tmpdir)
except FileExistsError:
    pass

batch = args.batchsize

idxs = list(range(n*batch,min((n+1)*batch,len(enspids))))
def getDisorder(i):
    ensp = enspids[i]
    ensp_dir = os.path.join(tmpdir,ensp)
    try:
        os.mkdir(ensp_dir)
    except FileExistsError:
        pass
    record = pep_dict[enspids[i]]
    seq_file = os.path.join(ensp_dir,"{}.flat".format(ensp))
    with open(seq_file, 'wt') as outseq:
        seq_file.write(str(record.seq))

    cmd = 'java -jar {} '.format(os.path.join(script_dir, 'VSL2.jar'))) +\
    '-s:{} > {} '.format(seq_file, os.path.join(outdir,'{}.disorder.out'.format(ensp)))
    subprocess.call(cmd, shell=True)
p = multiprocessing.Pool(processes=args.threads[0])
p.map_async(getDisorder, idxs)
p.close()
p.join()
