# Building custom annotations for CRISPRO
**WORK IN PROGRESS**

Welcome! This gitlab repository will contain all the code used for building and new annotation input files for [CRISPRO](https://gitlab.com/bauerlab/crispro).  

I will try and update this gitlab as I go, and keep the README updated to reflect the structure and content of this repository. Right now, it is mainly a collection of code snippets and scripts.

## Steps in compiling annotations
1. Download peptides/domains/guides
2. Generate annotations (provean/disorder/secondary structure)  
    This is what takes the longest --> trying to see if there is a better solution for downloading this data.  
    Approach: describePROT contains secondary structure, disorder scores and conservation. For quicker compilation this is what we'll use. The disorder scores are the same as the original annotations (VS2L), secondary structure is based on PSIpred, which was included in the original weighted secondary structure. Conservation are based on MMSeq2 instead of provean.
3. Create backbone with ensembl ids, AAs, and position
4. Blast proteins/domains (for PDB)
5. Add annotations to backbone

## Steps with scripts
1. **getEnsemblDomainCrispor.R**  
    input: list of genes (by ensembl ID)
    - pfam
    - smart (input: data_files/smart_ids.txt)
    - interpro 
    - exon-APPRIS-uniprot-gn-id
    - CRISPOR data
    - CCDS
    - peptide sequences

2. **interpro_graph_general.py**
    collapse interpro protein entries by greatest common ancestor;
    collapse related interpro entries via union
    Input: entry.list (from interpro)
    Input: ParentChildTreeFile.txt (from interpro) 
    output: interpro_domains.csv.gz

3. **SecStruct.py** 
    Download describeProt and parse. --> transferred into csv.gz
    needs to be parsed by uniprot ID
    
4. **guidesToAA2.py**
    Build (backbone) annotation file
    This script needs: crispor_file, pep file, cds file, exon file, domain file (interpro), provean file.

5. **BLAST_all.py** & **blastDomains.py**
    blasts proteins to get PDB annotations.

## Columns in final annotation file
1. ensembl_gene_id  
2. ensembl_transcript_id
3. ensembl_peptide_id
4. gene_name
5. transcript_name
6. APPRIS
7. position
8. AA
9. codon
10. guide
11. chrom
12. DSB_coordinate
13. guide_strand
14. distance_5_exon_border
15. distance_3_exon_border
16. Exon_multiple_of_3
17. EscsapeNMD
18. num_transcripts
19. targeted_transcripts
20. gene_strand
21. GuideGeneStrand
22. off_target_score
23. doench_score
24. CRISPRO_score
25. oof_score
26. gene_fraction
27. Exon
28. Exon_length
29. targeted_transcripts_frac
30. provean_score
31. disorder_score
32. Interpro_description
33. PDB_id
34. PDB_description
35. SecStruct
36. control



