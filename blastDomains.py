# -*- coding: utf-8 -*-
"""
Created on Mon Apr  2 13:22:19 2018

Author(s): Mitchel A. Cole, Vivien A. C. Schoonenberg

Take in interpro dataframe and pep fasta,
trim fasta file for domains
blast off
"""

import pandas as pd
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet.IUPAC import protein
from Bio import SeqIO
from Bio.Blast.Applications import NcbiblastpCommandline
from Bio.Blast import NCBIXML
import gzip
import re

E_VALUE_THRESH = 0.04
QUERY_COVERAGE = 0.6
IDENTITY = 0.75

# read in df
df = pd.read_csv("E:/Genomes/mm10/GRCm38.mmusculus.interpro-filtered.csv.gz")
with gzip.open("E:/Genomes/mm10/Mus_musculus.GRCm38.pep.all.fa.gz", "rt") as pepfile:
    seqs = SeqIO.to_dict(SeqIO.parse(pepfile, "fasta", protein))
seqs = {ensp.split('.')[0]: record for ensp, record in seqs.items()}
df = df[df["type"] == "Domain"]
# temporary filter for just taka's seqs
df_crispro = pd.read_csv("E:/projects/crispro/test/taka/CRISPRO.GRCm38.ensembl90.20180402.csv")
ensps = set(df_crispro["ensembl_peptide_id"].unique())
seqs = {ensp:record for ensp, record in seqs.items() if ensp in ensps}
seqs = [seq for _,seq in seqs.items()]

df = df[df["ensembl_peptide_id"].isin(ensps)]

seqs_lst = [val for _, val in seqs.items()]
# create domain seqrecords
for row in df.itertuples(index=False):
    pep = row.ensembl_peptide_id
    domain = row.description
    start = row.start
    end = row.end
    seq = str(seqs[pep].seq)[start-1:end]
    record = SeqRecord(Seq(seq, IUPACProtein),
                       id="{}_{}_{}_{}".format(pep, domain, start, end),
                       name=domain,
                       description="{} of {}_AA:{}-{}".format(domain, pep, start,end))
    seqs_lst.append(record)
seqs_lst.sort(key = lambda rec: rec.id)
### TODO: Separate seqs_lst into chunks
### TODO: make blast command

#parse xml
blast_data = []
with open("E:/projects/crispro/test/taka/GRCm38_taka_sm.blast_results.xml","rt") as xmlfile:
    blast_records = NCBIXML.parse(xmlfile)
    prog_ensp = re.compile("[A-Z0-9]+")
    prog_domain = re.compile("\S+ (\S+) of .*")
    prog_aa = re.compile(".*:(\d+)-(\d+)")
    for blast_record in blast_records:
        query_len = blast_record.query_length
        peptide = prog_ensp.match(blast_record.query).group()
        domainMatch = prog_domain.match(blast_record.query)
        peptideMatch = prog_aa.match(blast_record.query)
        if domainMatch:
            domain = domainMatch.group(1)
        else:
            domain = "complete peptide"
        if peptideMatch:
            aa_start = peptideMatch.group(1)
            aa_end = peptideMatch.group(2)
        else:
            aa_start = 1
            aa_end = query_len
        for alignment in blast_record.alignments:
            title = alignment.title
            for hsp in alignment.hsps:
                e_val = hsp.expect
                identity = hsp.identities
                align_len = hsp.align_length
                p_identity = float(identity) / float(align_len)
                p_coverage = float (align_len) / float(query_len)
                alignment_start = hsp.query_start
                alignment_end = alignment_start + align_len - 1
                if p_identity >= IDENTITY:
                    accession = alignment.accession
                    accession_split = accession.split('_')
                    PDB = accession_split[0]
                    chain = accession_split[1]
                    blast_rec = [peptide,
                                    domain,
                                    str(PDB),
                                    str(chain),
                                    int(aa_start),
                                    int(aa_end),
                                    int(alignment_start),
                                    int(alignment_end),
                                    int(align_len),
                                    int(query_len),
                                    str(e_val),
                                    float(p_identity),
                                    float(p_coverage),
                                    str(title)]
                    blast_data.append(blast_rec)
blast_cols = ['ensembl_peptide_id', 'domain', 'PDB',
              'chain', 'peptide_start', 'peptide_end','alignment_start', 
              'alignment_end', 'alignment_length', 'query_length',
              'e-value', 'identity', 'coverage', 'description']
df_blast = pd.DataFrame(blast_data,
                        columns=blast_cols)
df_blast = pd.merge(df_blast,
                    df.iloc[:,1:4].drop_duplicates(),
                    how="left", on="ensembl_peptide_id")
pdb_ids = []
for row in df_blast.itertuples(index=False):
    ensp = row.ensembl_peptide_id
    aa_start = row.peptide_start + row.alignment_start - 1
    aa_end = aa_start + row.alignment_length
    pdb = row.PDB
    description = row.description
    for aa in range(aa_start, aa_end + 1):
        pdb_ids.append([ensp,aa, pdb, description])
        
df_pdb = pd.DataFrame(pdb_ids, columns = ["ensembl_peptide_id",
                                          "pos", "PDB","PDB_Description"])
df_pdb.drop_duplicates(inplace=True)
df_pdb = df_pdb.groupby(['ensembl_peptide_id','pos']).agg(lambda s: s.str.cat(sep="/"))
df_crispro2 = pd.merge(df_crispro, df_pdb,
                       how="left", left_on=["ensembl_peptide_id","pos"],
                       right_index=True)